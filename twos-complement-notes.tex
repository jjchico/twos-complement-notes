\documentclass[english,a4paper,12pt,titlepage]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{natbib}
\usepackage{chngcntr}
\usepackage{authblk}
\usepackage{hyperref}

\newtheorem{theorem}{Theorem}%[section]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\theoremstyle{definition}
\newtheorem{definition}{Definition}%[section]
\newtheorem{example}{Example}%[section]
\theoremstyle{remark}
\newtheorem{case}{Case}

\counterwithin*{case}{theorem}

\newcommand{\bbZ}{\mathbb{Z}}

\title{Two's complement binary representation of integer numbers}
\author{Jorge Juan-Chico}
\affil{\textit{jjchico@dte.us.es}}
\affil{Departamento de Tecnología Electrónica.\\Universidad de Sevilla}
%\date{Octubre, 2016}

\begin{document}

\maketitle
\newpage
\tableofcontents
\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\phantomsection{}
\addcontentsline{toc}{section}{Preface}
\section*{Preface}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This document contains a set of useful definitions and properties to introduce the two's complement representation of integer number. It aims at providing precise definitions and demonstrations of the properties and theorems involved with only brief notes on application and usefulness.

The content of the document is a personal view of the author about the matter. It is used by the author as a formal background to support its teaching about number representation including two's complement representation.

This work is licensed under Attribution-ShareAlike 4.0 International. You are free to share and modify this work provided you cite the original author and share the derived work with the same license. To view a copy of this license, visit \url{https://creativecommons.org/licenses/by-sa/4.0/}.

You may find the source code of the last version of this document at \url{https://gitlab.com/jjchico/twos-complement-notes}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Positional representation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This section introduces some basic properties of the positional representation of numbers that will be used in later sections.

\begin{definition}[Positional representation of integer numbers]
 Given $x \in \bbZ, x \ge 0$, the positional representation of $x$ in the numeric base $b\in\bbZ, b>1$, is defined by the set of digits $\{x_{n-1}, x_{n-2}, \ldots, x_1, x_0\}$ with $x_i \in
 \bbZ, 0 \le x_i < b$, so that:
  \[
    x = x_{n-1} b^{n-1}+x_{n-2} b^{n-2}+ \cdots + x_1 b^1+x_0
  \]
\end{definition}

\begin{theorem}\label{theorem-maxn}
  The biggest number $x \in \bbZ, x \ge 0$, that can be represented with $n$ digits in base $b$ is
  \[
    x = b^n - 1
  \]

  \begin{proof}
    Considering that the digits $x_i$ that form the representation of $x$ in base $b$ are all smaller than $b$, the biggest number that can be represented is that obtained when all the digits in the representation are set to their maximum value $b-1$, therefore, the biggest number that can be represented with $n$ digits is:
    \begin{align*}
      \sum_{i=0}^{n-1} (b-1)b^i
      = \sum_{i=0}^{n-1} \left(b^{i+1}-b^i\right)
      = \sum_{i=0}^{n-1} b^{i+1} - \sum_{i=0}^{n-1} b^i\\
      = \sum_{i=1}^{n} b^{i} - \sum_{i=0}^{n-1} b^i
      = b^n + \sum_{i=1}^{n-1} b^{i} - \sum_{i=1}^{n-1} b^i - 1
      = b^n - 1
    \end{align*}
  \end{proof}
\end{theorem}

\begin{corollary}\label{corollary-pos-digit}
  The weight of each unit of the $i$-st digit in a positional representation in base $b$ is greater than the maximum value that can be represented with the rest of the less significant digits.
  \begin{proof}
    The weight of each unit of the $i$-st digit is $b^i$ and according to Theorem~\ref{theorem-maxn}, the maximum representable value with the $i-1$ digits less significant than digit $i$ is $b^i-1$, that is less than $b^i$ as we wanted to prove.
  \end{proof}
\end{corollary}

\begin{theorem}\label{theorem-pos-uniq}
  Given $x \in \bbZ, x \ge 0$, the positional representation of $x$ in base $b$ is unique.
  \begin{proof}
    Let us assume that there are two different representation of $x$ in base $b$, that is:
    \[
      x = \sum_{i=0}^{n-1} x_i b^i = \sum_{i=0}^{n-1} x'_i b^i
    \]
    so that $x_i \neq x'_i$ for some $i, 0 \le i \le n-1$. Let $j$ be the greatest index so that $x_j \neq x'_j$, it holds that:
    \begin{align*}
      |x_j-x'_j| \ge 1 \\
      x = \sum_{i=j+1}^{n-1} x_i b^i + x_j b^j + \sum_{i=0}^{j-1} x_i b^i
        = \sum_{i=0}^{j-1} x_i b^i + x'_j b^j + \sum_{i=0}^{j-1} x'_i b^i \\
      x-x = 0 = (x_j-x'_j) b^j + \sum_{i=0}^{j-1} (x_i-x'_i)b^i \\
      \left| (x_j-x'_j) b^j \right| =
        \left| \sum_{i=0}^{j-1} (x_i-x'_i)b^i \right|
    \end{align*}
    It yields:
    \begin{align*}
      b^j \le \left| (x_j-x'_j) b^j \right|
        = \left| \sum_{i=0}^{j-1} (x_i-x'_i)b^i \right| \le
        \sum_{i=0}^{j-1} |(x_i-x'_i)|b^i \\
        \le \sum_{i=0}^{j-1} (b-1)b^i = b^j - 1
    \end{align*}
    but it is a contradiction, so we conclude that there is not a greatest index $j$ so that $x_j \neq x'_j$ and, therefore, there are not different digits in two positional representations of $x$.
  \end{proof}
\end{theorem}

\subsection{Useful terms}

\begin{description}
  \item [Bit:] Base 2 digit. It comes from \emph{BInary digiT}.
  \item [\textit{n}-bit binary word:] Ordered set of $n$ bits.
  \item [\textit{n}-bit positional representation:] Positional representation in base 2 using $n$ digits.
  \item [Binary word's magnitude:] Integer value of the bits in the binary word when interpreted as the digits of a positional representation in base 2.
\end{description}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Two's complement representation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The \emph{Two's complement representation} is one of the methods used to represent signed integer numbers by using binary words. Other popular methods are \emph{Sign-and-magnitude representation}, \emph{Ones complement representation} and \emph{Excess representations}.

Two's complement representation has the advantage to simplify the design of arithmetic circuits that operate with signed integer numbers. For this reason, the two's complement representation is the most extended method to code signed integer numbers in computers and computer languages.

In this section, the two's complement representation is defined and its main properties are introduced.

\begin{definition}[Two's complement representation]\label{def-tcr}
  Given $x \in \bbZ$, $-2^{n-1} \le x \le 2^{n-1}-1$, the two's complement representation of $x$ with $n$ bits is an $n$-bit binary word which magnitude, $TCR_n(x)$, is:
  \[
    TCR_n(x) =
      \begin{cases}
        x     & \text{if } x \ge 0\\
        2^n+x & \text{if } x < 0
      \end{cases}
  \]
\end{definition}

\begin{definition}\label{def-tcr_rep}
  Is is said that $x$ is representable in two's complement  with $n$ bits if, and only if, $-2^{n-1} \le x \le 2^{n-1}-1$.
\end{definition}

\begin{definition}[Two's complement of an integer number]
  Given $x \in \bbZ, 0 \le x \le 2^n-1$, the two's complement with $n$ bits of $x$, $TC_n(x)$, is defined as:
  \[
    TC_n(x) = 2^n - x
  \]
\end{definition}

\begin{corollary}
  The magnitude of the two's complement representation with $n$ bits of $x$, $TCR_n(x)$, can be expressed as:
  \[
    TCR_n(x) = \begin{cases}
      x        & \text{if } x \ge 0\\
      TC_n(-x) & \text{if } x < 0
    \end{cases}
  \]
\end{corollary}

\begin{theorem}\label{theorem-sign-bit}
  If $x$ is representable in two's complement with $n$ bits, the most significant bit of the two's complement representation of $x$ is 0 if $x \ge 0$, and 1 if $x < 0$.
  \begin{proof}
    \begin{case}
      $x \ge 0$.
      \[TCR_n(x)= x = x_{n-1}2^{n-1}+x_{n-2}2^{n-2}+\cdots+x_0\]
      Since $x$ is representable in two's complement with $n$ bits, from definition~\ref{def-tcr_rep} it yields $x<2^{n-1}$ and, from corollary~\ref{corollary-pos-digit}, it holds that $x_{n-1}=0$.
    \end{case}
    \begin{case}
      $x<0$.
      \[
        TCR_n(x)= 2^n+x = x_{n-1}2^{n-1}+x_{n-2}2^{n-2}+\cdots+x_0
      \]
      Since $x$ is representable in two's complement with $n$ bits, from definition~\ref{def-tcr_rep} it yields
      \[
        -2^{n-1} \le x < 0  \qquad 2^{n-1} \le 2^n + x < 2^n
      \]
      and, from corollary~\ref{corollary-pos-digit}, it holds that $x_{n-1}=1$.
    \end{case}
  \end{proof}
\end{theorem}

\begin{definition}[Sign bit in the two's complement representation]
  Given $x \in \bbZ$, we define the \emph{sign bit} of the two's complement representation with $n$ bits as the most significant bit of the two's complement represetnation with $n$ bits of $x$.
\end{definition}

\begin{theorem}[Opposite calculation]
  Given $x \in \bbZ$ representable in two's complement with $n$ bits, $x \ne 0$ and $x \ne -2^{n-1}$, the magnitude of the two's complement representation of $-x$ is the two's complement of the magnitude of the two's complement representation of $x$, provided that $-x$ is representable in two's complement. That is:
  \[TCR_n(-x) = TC_n(TCR_n(x))\]
  In other words, the two's complement representation of $-x$ may be obtained simply by computing the two's complement of the two's complement representation of $x$.

  \begin{proof}
    If $0<x<2^{n-1}$, it hods that $TCR_n(x)=x$ and, additionally:
    \[
      TCR_n(-x) = 2^n - x = TC_n(x) = TC_n(TCR_n(x))
    \]
    On the other hand, if $-2^{n-1}<x<0$, it holds that $TCR_n(x)=2^n+x$ and, additionally:
    \[
      TCR_n(-x) = -x = 2^n - 2^n - x = 2^n - (2^n+x) = TC_n(TCR_n(x))
    \]
  \end{proof}
\end{theorem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Addition and overflow in two's complement representation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The main advantage of the two's complement representation is that it is very
easy to add numbers using their two's complement representation directly. The next theorem introduces the fundamental property of the two's complement addition and it basically says that the two's complement representation of $x+y$ can be obtained by just adding the two's complement representations of $x$ and $y$ as if they were regular positive integer numbers.

\begin{theorem}[Two's complemnt addition rule]\label{theorem-sum-law}
  Given $x, y \in \bbZ$ so that $x$, $y$ and $x+y$ are representable in two's complement, it holds that:
  \[
    TCR_n(x+y) = [TCR_n(x) + TCR_n(y)] \mod 2^n
  \]
  \begin{proof}
    Let us distinguish various cases:

    \begin{case}
      $0 \le x,y$

      Since $x$ and $y$ are positive numbers, by using definition~\ref{def-tcr} we have that:
      \[
        [TCR_n(x)+TCR_n(y)] \mod 2^n = (x + y) \mod 2^n
      \]
      On the other hand, since $x+y$ is positive and representable in two's complement it holds that $0 \le x + y < 2^{n-1}$ and hence:
      \[
        [TCR_n(x)+TCR_n(y)] \mod 2^n = x + y = TCR_n(x+y)
      \]
    \end{case}

    \begin{case}
      $x \ge 0; y < 0; x+y \ge 0$

      \[
        [TCR_n(x)+TCR_n(y)] \mod 2^n = (x + 2^n + y) \mod 2^n
      \]
      Since $x+y \ge 0$, it holds that:
      \[
        [TCR_n(x)+TCR_n(y)] \mod 2^n = (x + y) = TCR_n(x+y)
      \]
    \end{case}

    \begin{case}
      $a \ge 0; b < 0; x+y < 0$

      \[
        [TCR_n(x)+TCR_n(y)] \mod 2^n = (x + 2^n + y) \mod 2^n
      \]
      Since $x+y < 0$, it holds that:
      \[
        [TCR_n(x)+TCR_n(y)] \mod 2^n = 2^n + x + y = TCR_n(x + y)
      \]
    \end{case}

    \begin{case}
      $x < 0; y < 0$

      \[
        [TCR_n(x)+TCR_n(y)] \mod 2^n = (2^n + x + 2^n + y) \mod 2^n
      \]

      Since both $x$ and $y$ are representables in two's complement it holds that $-2^{n-1} < x+y < 0$, therefore, $-2^n < x+y < 0$
      and $0 < 2^n + x + y < 2^n$, hence:
      \[
        [TCR_n(x)+TCR_n(y)] \mod 2^n = 2^n + x + y = TCR_n(x + y)
      \]
    \end{case}
  \end{proof}
\end{theorem}

\begin{definition}[Two's complement overflow]
  Given $x, y \in \bbZ$ representable in two's complement with $n$ bits, it is said that the two's complement addition with $n$ bits of $x$ and $y$ generates an overflow if and only if $x+y$ is not representable in two's complement with $n$ bits.
\end{definition}

\begin{theorem}[Two's complement overflow rule]\label{theorem-overflow-law}
  Given $x, y \in \bbZ$ representable in two's complement with $n$ bits, the addition $x+y$ generates overflow if and only if the sign bits of the two's complement representations of $x$ and $y$ are the same but different from the most significant bit of $s = [TCR_n(x)+TCR_n(y)] \mod 2^n$.
  \begin{proof}
    Let us distinguish two cases:

    \begin{case}
      $0 \le x,y$

      In this case, $x$ and $y$ have the same sign bit equal to $0$. Since $x$ and $y$ are positive integers representable in two's complement, it holds that $0 \le x+y < 2^n$ y $s = (x + y)$.

      If $x+y < 2^{n-1}$, $x+y$ is representable in two's complement with $n$ bits, its most significant bit is $0$ and is equal to the sign bits of $x$ and $y$.

      On the contrary, if $2^{n-1} \le x+y < 2^n$, $x+y$ is not representable in two's complement with $n$ bits, its most significant bit is $1$ and it is not equal to the sign bits lf $x$ and $y$.
    \end{case}

    \begin{case}
      $x \ge 0; y < 0$

      This case is equivalent to the case in which $y \ge 0; x < 0$ that has an equivalent demonstration.

      In this case, $x$ and $y$ have different sign bits. Since $x$ and $y$ are representable in two's complement with $n$ bits, it holds that $0 \le x < 2^{n-1}$ y $2^{n-1} \le y < 0$, hence:

      \[
        2^{n-1} \le x + y < 2^{n-1}
      \]

      Therefore $x+y$ is representable in two's complement with $n$ bits and the addition of $x$ and $y$ does not generates overflow.
    \end{case}

    \begin{case}
      $x < 0; y < 0$

      In this case, $x$ and $y$ have the same sign bit equal to $1$. Since $x$ and $y$ are negative integers representable in two's complement, it holds that $s = (2^n + x + 2^n + y) \mod 2^n$ y $-2^n \le x+y < 0$. Hence:
      \begin{align*}
        2^n + 2^n - 2^n-1 \le 2^n + 2^n + x + y < 2^n + 2^n\\
        2^n \le 2^n + 2^n + x + y < 2^n + 2^n\\
        s = (2^n + 2^n + x + y) \mod 2^n = 2^n + x + y
      \end{align*}

      If $-2^{n-1} \le x+y < 0$, $x+y$ is representable in two's complement with $n$ bits and it holds that $2^{n-1} \le s < 2^n$ and, therefore, the most significant bit of $s$ is $1$ and equal to the sign bits of $x$ and $y$.

      On the other hand, if $-2^n \le x+y < -2^{n-1}$, $x+y$ is not representable in two's complement with $n$ bits and it holds that $0 \le s < 2^{n-1}$ and, therefore, the most significant bit of $s$ is $0$ and is not equal to the sign bits of $x$ and $y$.
    \end{case}
  \end{proof}
\end{theorem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Using magnitude adders with numbers represented in two's complement}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The good properties of the two's complement addition have an important application to design adder circuits efficiently. Thanks to the properties of the addition in two's complement, the design of adder circuits, and other arithmetic circuitos thereafter, is greatly simplified. Below, some definitions and properties related to the two's complement addition with application to the design of adder circuits are introduced.

\begin{definition}[Magnitudes adder of $n$-bit numbers]
  A magnitudes adder of $n$-bit numbers is a system with two $n$-bit inputs that encode the magnitudes (positive integers) $x$ and $y$ en base 2, and one $n$-bit output $z$ that codifies in base 2 the result of adding $x$ and $y$ so that:
  \[
    z = (x + y) \mod 2^n
  \]
\end{definition}

The following corollary is a direct application of the two's complement addition rule to the magnitudes adders. It basically says that it is possible to use a magnitudes adder, initially designed to add positive integer numbers, to add positive and negative integers by just using the two's complement representation of the numbers as inputs to the adder.

\begin{corollary}
  A $n$-bit magnitudes adder which operands are the two's complement representations with $n$ bits of $x, y \in \bbZ$, will output the two's complement representation with $n$ bits of $x+y$ provided that $x+y$ is representable in two's complement with $n$ bits.
  \begin{proof}
    If the inputs to the magnitudes adder are the two's complement representations of $x$ and $y$ with $n$ bits, according to its definition, the adder will generate the following output value:
    \[
      z = [TCR_n(x)+TCR_n(y)] \mod 2^n
    \]
    But according to theorem~\ref{theorem-sum-law}, if $x+y$ is representable in two's complement with $n$ bits, we have that:
    \[
      z = TCR_n(x + y)
    \]
  \end{proof}
\end{corollary}

The next corollary is a practical method to detect when the addition operation done by a magnitudes adder using input in two's complement representation generates an overflow. From this result, it is easy to design a digital circuit that detects this overflow condition.

\begin{corollary}
  The addition of the two's complement representations of $n$-bit numbers $x, y \in \bbZ$ performed by an $n$-bit magnitudes adder will generate an overflow if, and only if, the sign bits of $x$ and $y$ are the same but different from the most significant bit produced by the adder.
  \begin{proof}
    Considering that the output $z$ of the adder is:
    \[
      z = [TCR_n(x)+TCR_n(y)] \mod 2^n
    \]
    this corollary is just an alternative wording of theorem~\ref{theorem-overflow-law}.
  \end{proof}
\end{corollary}

%% TODO: Overflow detection by using the carry bits. It would be necessary to introduce the concept of carry bit in a previous section about binary arithmetic.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Ones complement and relation to two's complement}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The ones complement of integer numbers is similar operation to the two's complement. From the definition of the ones complement operation, it can be defined a ones complement representation for integer numbers with similar characteristics and properties to the two's complement representation. Some digital systems (specially older ones) used this representations instead of the two's complement representation. In this document, the ones complement operation is introduced but used only as a practical tool to calculate the two's complement of a number directly from its binary representation.

\begin{definition}[Ones complement of an integer number]\label{def-c1}
  Given $x \in \bbZ, 0 \le x \le 2^n-1$, the ones complement with $n$ bits of $x$, $OC_n(x)$, is defined as the magnitude that is obtained by substituting all the $1$ digits with $0$ and all the $0$ digits with $1$ in the positional representation of $x$ in base 2 with $n$ digits.
\end{definition}

The next theorem allow us to calculate the ones complement of a number algebraically without needing to use the base 2 representation.

\begin{proposition}
  Given $x \in \bbZ, 0 \le x \le 2^n-1$, it holds that:
  \[
    OC_n(x) = 2^n - x - 1
  \]
  \begin{proof}
    Since the $OC_n(x)$ is obtained by complementing all the bits in the base 2 representation of $x$, it is easy to see that the addition in base 2 of $x+OC_n(x)$ results in a base 2 number with all its $n$ bits equal to $1$, which is the maximum number that can be represented with $n$ bits or $2^n-1$ according to theorem~\ref{theorem-maxn}. Hence:
    \begin{align*}
        x + OC_n(x) = 2^n - 1\\
        OC_n(x) = 2^n - x - 1
    \end{align*}
  \end{proof}
\end{proposition}

The next corollary is obtained directly from the two's complement definition and provides a method to calculate the two's complement of a number in binary format.

\begin{corollary}[Relation between the ones complement and the two's complement]\label{oc-tc-relation}
  Given $x \in \bbZ, 0 \le x \le 2^n-1$, it holds that:
  \begin{align*}
    OC_n(x) = TC_n(x) - 1\\
    TC_n(x) = OC_n(x) + 1
  \end{align*}
\end{corollary}

The proposition below provides an even faster method to obtain the two's complement of a number from its binary representation.

\begin{proposition}
  Given $x \in \bbZ, 0 \le x$ expressed in base 2, the two's complement with $n$ bits of $x$ can be obtained by using the following procedure: starting at the less significant bit and advancing to the most significant bit, all bits that are $0$ are copied until reaching the first bit that is $1$. This first bit at $1$ is also copied and the rest of the bits are copied after computing their complement.
  \begin{proof}
    The demonstration of this proposition is based on corollary~\ref{oc-tc-relation}. The operation $OC_n(x)+1$ will convert to 0 all the bits at 1 of $OC_n(x)$ that are consecutive and aligned to the right of the number, as a result of adding 1 to the right-most bit at 1 and the propagation of the carry bit. The propagation of successive carry bits will stop when the first bit at 0 is reached, that will be converted to 1 in the result due to the propagated carry bit. The rest of the bits will not be changed by the addition operation because there is not more carry bits to propagate. This way, to obtain $TC_n(x) = OC_n(x)+1$ from $OC_n(x)$ consists on complementing all the bits at 1 of $OC_n(x)$ starting from the right-most bit until reaching the first bit at 0, which is also complemente, and leaving the rest of the bits unchanged.

    But $TC_n(x) = OC_n(x)+1$ may also be obtained directly from $x$ by taking into account that each bit of $x$ is the complement of the corresponding bit of $OC_n(x)$ according definition~\ref{def-c1} and reversing the above described operation on the bits of $OC_n(x)$. That is, leaving all the right-most bits at 1 of $x$ unchanged up to the first bit at 0 (included) and complementing the rest of the bits.
  \end{proof}
\end{proposition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Bit width extension and reduction in two's complement}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A frequent operation when representing numbers in binary format is to increment or decrement the number of bits used by the representation without changing the value of the represented number. The properties in this section are mechanisms to extend or reduce the bit width of a two's complement representation working on the bits of the representation themselves, which are useful to be implemented by digital circuits.

The next theorem says that the two's complement representation of a number $x$ with $n+1$ bits can be obtained by adding a new sign bit that is equal to the sign bit of the two's complement representation of $x$ with $n$ bits.

\begin{theorem}[Sign extension in two's complement representation]
  Given $x \in \bbZ$ representable in two's complement with $n$ bits, $TCR_n(x)$ the two's complement representation of $x$ with $n$ bits and $s$ the sign bit of $x$, it holds that:
  \[
      TCR_{n+1}(x) = s 2^n + TCR_n(x)
  \]
  \begin{proof}
      We will distinguish two cases:
    \begin{case}
      $0 \le x < 2^{n-1}$

      Since $0 \le x < 2^{n-1}$ it holds that $0 \le x < 2^n$ hence $x$ is representable in two's complement with $n+1$ bits. Considering definition~\ref{def-tcr}, we have that:
      \[
        TCR_{n+1}(x) = TCR_n(x) = x
      \]

      And since $x \ge 0$, due to theorem~\ref{theorem-sign-bit} the sign bit of $TCR_n(x)$ is $s=0$, hence:
      \[
        RC2_{n+1}(x) = s 2^n + TCR_n(x)
      \]
    \end{case}
    \begin{case}
      $-2^{n-1} \le x < 0$

      Since $-2^{n-1} \le x < 0$ it also holds that $-2^n \le x < 0$ hence $x$ is representable in two's complement with $n+1$ bits. Considering definition~\ref{def-tcr}, we have that:
      \[
        TCR_{n+1}(x) = 2^{n+1} + x = 2^n + 2^n + x\\
        TCR_n(x) = 2^n + x
      \]
      Then:
      \[
        TCR_{n+1}(x) = 2^n + TCR_n(x)
      \]
      And since $x < 0$, due to theorem~\ref{theorem-sign-bit} the sign bit of $TCR_n(x)$ is $s=1$, hence:
      \[
        RC2_{n+1}(x) = s 2^n + TCR_n(x)
      \]
    \end{case}
  \end{proof}
\end{theorem}

\begin{corollary}
  Given $x \in \bbZ$ representable in two's complement with $n$ bits, $x$ is representable in two's complement with $n-1$ bits if, and only if, the two most significant bits of the two's complement representation of $x$ with $n$ bits are the same, that is, if the sign bit does not change when taking one bit from the two's complement representation of the number.
\end{corollary}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Two's complement representation as a\\weighted code}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The following theorem is very interesting because it demonstrates that the two's complement representation is also a weighted code similar to the positional representation in base 2. This result may be used as an alternative definition for the two's complement representation that avoids the introduction of the complement operations.

The following theorem also provides a practical way to operate with the two's complement representation by hand without having to apply the two's complement operation, specially when the number of bits is not too big (like 8 bits).

\begin{theorem}[Two's complement representation as a wighted code]
  Given $x \in \bbZ$ representable in two's complement with $n$ bits and $TCR_n(x)$ the magnitude of the two's complement representation of $x$ with $n$ bits, formed by the binary digits $\{x_{n-1}, x_{n-2}, \ldots, x_1, x_0\}$, it holds that:
  \[
      x = x_{n-1} (-2^{n-1})+x_{n-2} 2^{n-2}+ \cdots + x_1 2^1+x_0
  \]
  \begin{proof}
    Let us distinguish two cases.

    \begin{case}
      $0 \le x < 2^{n-1}$

      From definition~\ref{def-tcr} it holds that $TCR_n(x) = x$. By using theorem~\ref{theorem-sign-bit}, we know that $x_{n-1}$ is the sign bit of $x$ and it is equal to 0, hence:
      \begin{align*}
        x = x_{n-2} 2^{n-2}+ \cdots + x_1 2^1+x_0\\
        x = x_{n-1} (-2^{n-1})+x_{n-2} 2^{n-2}+ \cdots + x_1 2^1+x_0
      \end{align*}
    \end{case}
    \begin{case}
      $-2^{n-1} \le x < 0$

      From definition~\ref{def-tcr} it holds that $TCR_n(x) = 2^n + x$. By using theorem~\ref{theorem-sign-bit}, $x_{n-1}$ is the sign bit of $x$ and it is equal to 1, hence:
      \[
        TCR_n(x) = 2^n + x = 2^{n-1}+x_{n-2}2^{n-2}+\cdots+x_1 2^1+x_0
      \]
      By isolating $x$ we have that:
      \[
        x = 2^{n-1}-2^n + x_{n-2} 2^{n-2}+ \cdots +x_1 2^1+x_0
      \]
      Taking into account that $2^{n-1}-2^n = -2^{n-1}$ and that $x_{n-1}=1$, it holds that:
      \[
        x = x_{n-1} (-2^{n-1})+x_{n-2} 2^{n-2}+ \cdots + x_1 2^1+x_0
      \]
    \end{case}
  \end{proof}
\end{theorem}
\end{document}
