# Two's complement binary representation of integer numbers

EThis document contains a set of useful definitions and properties to introduce the two's complement representation of integer number. It aims at providing precise definitions and demonstrations of the properties and theorems involved with only brief notes on application and usefulness.

## Document generation

You can generate a PDF document from the source code of this document by using the LaTeX text processing system. For instance, in systems that support LaTeX and have the *latexmk* command installed you may execute the following command:

    $ latexmk -pdf twos-complement-notes.tex

Alternatively, you may download an automatically generated pdf from https://gitlab.com/jjchico/twos-complement-notes/-/jobs/artifacts/main/browse?job=build_pdf

## Contributions

You may close this repository, modify it and send merge requests that will be evaluated by the author.

## Author

* Jorge Juan-Chico <jjchico@dte.us.es>

## License

This work is licensed under Attribution-ShareAlike 4.0 International. You are free to share and modify this work provided you cite the original author and share the derived work with the same license. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/.
